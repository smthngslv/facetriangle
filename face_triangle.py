import cv2
import dlib
import imutils

import numpy as np

from imutils import face_utils

PARTS = (("mouth", (48, 68)),("right_eyebrow", (17, 22)), ("left_eyebrow", (22, 27)),	("right_eye", (36, 42)), ("left_eye", (42, 48)),	("nose", (27, 35)))

def lineEq(p1, p2):
    k = (p1[1] - p2[1])/(p1[0] - p2[0])
    
    
    b1 = p1[1] - p1[0] * k
    b2 = p2[1] - p2[0] * k
    
    return (k, (b1 + b2) / 2)

def getTriangle(eq, base, up, size = 150, w = 2):
    a = 1 + eq[0]**2
    b = -(2 * base[0] - 2 * eq[0] * (eq[1] - base[1]))
    c = base[0]**2 + (eq[1] - base[1]) ** 2 - size ** 2
    
    d = (b**2 - 4 * a * c) ** 0.5
    
    x1 = (-b + d)/(2* a)
    x2 = (-b - d)/(2* a)

    y1 = eq[0] * x1 + eq[1]
    y2 = eq[0] * x2 + eq[1]
    
    #print(up, base)
    
    x3 = (up[0] - base[0]) * w + base[0]
    y3 = (up[1] - base[1]) * w + base[1]    
    
    return ((int(x1), int(y1)), (int(x2), int(y2)), (int(x3), int(y3)))

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector  = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

cam = cv2.cv2.VideoCapture(0)
#cam = cv2.cv2.VideoCapture(r'C:\Users\fsoci\Desktop\Zoom 2020-04-09 15-58-46.mp4')

while(cv2.waitKey(1) != 27):
    _, image = cam.read()
    
    if image is None:
        print('No frame.')

        continue

    image = imutils.resize(image, width=500)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # detect faces in the grayscale image
    rects = detector(gray, 1)
    
    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the landmark (x, y)-coordinates to a NumPy array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        
        parts = {}
       
        # loop over the face parts individually
        for (name, (i, j)) in PARTS:
            # clone the original image so we can draw on it, then
            # display the name of the face part on the image
            #clone = image
            #cv2.putText(clone, name, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
            #    0.7, (0, 0, 255), 2)

            # loop over the subset of facial landmarks, drawing the
            # specific face part
            #for (x, y) in shape[i:j]:
            #    cv2.circle(clone, (x, y), 1, (0, 0, 255), -1)

            # extract the ROI of the face region as a separate image
            (x, y, w, h) = cv2.boundingRect(np.array([shape[i:j]]))
            roi = image[y:y + h, x:x + w]
            roi = imutils.resize(roi, width=int(w), inter=cv2.INTER_CUBIC)

            parts[name] = ((x, y, w, h), roi)

            # show the particular face part
            #cv2.imshow(name, roi)
            #cv2.imshow(name, clone)

        # visualize all facial landmarks with a transparent overlay
        #output = face_utils.visualize_facial_landmarks(image, shape)
        
        eq = lineEq(shape[6], shape[9])
            
        p1, p2, p3 = getTriangle(eq, shape[8], shape[27])
        
        pts = np.array([p1, p2, p3], np.int32)
        pts = pts.reshape((-1,1,2))
        cv2.fillPoly(image, [pts], (0,255,255))   
        
        for k in parts.keys():        
            x, y, w, h = parts[k][0]
            
            if k == 'mouth':
                p1 = shape[3]
                p2 = shape[48]
            
                x1 = (p1[0] - p2[0]) * 2 + p2[0]
                y1 = (p1[1] - p2[1]) * 2 + p2[1]
                
                
                image[y1:y1 + h, x1:x1 + w] = parts[k][1]
            
            if k == 'right_eye':
                p1 = shape[23]
                p2 = shape[45]
            
                x1 = int(-(p1[0] - p2[0]) * 1.2 + p2[0])
                y1 = int(-(p1[1] - p2[1]) * 1.2 + p2[1])
                
                
                image[y1:y1 + h, x1:x1 + w] = parts[k][1]
            
            
            if k == 'left_eye':
                p1 = shape[20]
                p2 = shape[36]
            
                x1 = int(-(p1[0] - p2[0]) * 1.2 + p2[0])
                y1 = int(-(p1[1] - p2[1]) * 1.2 + p2[1])
                
                
                image[y1:y1 + h, x1:x1 + w] = parts[k][1]
            
            if k == 'right_eyebrow':
                p1 = shape[23]
                p2 = shape[45]
            
                x1 = int((p1[0] - p2[0]) * 1.2 + p2[0])
                y1 = int((p1[1] - p2[1]) * 1.2 + p2[1])
                
                
                image[y1:y1 + h, x1:x1 + w] = parts[k][1]
            
            
            if k == 'left_eyebrow':
                p1 = shape[20]
                p2 = shape[36]
            
                x1 = int((p1[0] - p2[0]) * 1.2 + p2[0])
                y1 = int((p1[1] - p2[1]) * 1.2 + p2[1])
                
                
                image[y1:y1 + h, x1:x1 + w] = parts[k][1]
            
            if k == 'nose':
                p1 = shape[31]
                p2 = shape[35]
            
                x1 = int((p1[0] - p2[0]) * 1.2 + p2[0])
                y1 = int((p1[1] - p2[1]) * 1.2 + p2[1])
                
                
                image[y1:y1 + h, x1:x1 + w] = parts[k][1]
    
    image = imutils.resize(image, width=1000, inter=cv2.INTER_CUBIC)
    
    cv2.imshow("Image", image)